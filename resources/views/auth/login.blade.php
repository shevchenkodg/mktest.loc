@extends('auth.layout')

@section('main')
  <div class="card bordered z-depth-2 animated  bounceIn" style="margin:0% auto; max-width:400px;">
    <div class="card-header">
      <div class="brand-logo">
        <i class="md md-dashboard" style="font-size: 24px;"></i> {{ Request::server('HTTP_HOST') }} </div>
    </div>
    <div id="forms-validation-container">
      <form class="form-floating" id="form-validation" method="POST" action="/auth/login">
        <div class="card-content">
          @if (count($errors) > 0)
            <div class="m-b-30">
              <div class="bs-component">
                <div class="alert alert-dismissible alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <h4>Ошибка</h4>
                  @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                  @endforeach
                </div>
              </div>
            </div>
          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
            <label for="inputEmail" class="control-label">Email</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required="" data-error="Введите корректный email адрес">
            <div class="help-block with-errors"></div>
          </div>
          <div class="form-group">
            <label for="inputPassword" class="control-label">Пароль</label>
            <input type="password" class="form-control" id="inputPassword" name="password" required="" data-error="Введите пароль">
            <div class="help-block with-errors"></div>
          </div>
          <div class="form-group">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="remember"> Запомнить меня </label>
            </div>
          </div>
        </div>
        <div class="card-action clearfix">
          <div class="pull-right">
            <a href="/password/email" class="btn btn-link black-text">Забыли пароль?</a>
            <button type="submit" class="btn btn-link black-text">Войти</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop