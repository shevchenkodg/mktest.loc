<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Config;

class VerifySuperadminAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role <> 'superadmin') {
            $ccPrefix = Config::get('app.ccPrefix');
            return redirect($ccPrefix);
        }
        
        return $next($request);
    }
}
