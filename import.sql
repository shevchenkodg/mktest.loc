-- CMS mandatory records | Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `users` (`id`, `name`, `email`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Test User',  'test@test.test', 'superadmin', '$2y$10$r8GG6yN1zyLTbbp7o1e35u7dgnCOybeIQvNh6sruuhSEG5n6QccB2', '5GdQpr3rAFoEqA8URJkW1tAcdfr8VnbcaAxrcqZMBd0JPSijN8Lme2Q4ibqO', '2016-07-15 08:34:16',  '2016-11-02 16:30:30');

-- 2016-11-04 13:51:00
