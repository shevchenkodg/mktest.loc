$(function() {
  // Выделить все
  $("#checkAllItems").on('click', function() {
    $('input[type="checkbox"][name*="items"]').prop('checked', $('input[type="checkbox"][name*="items"]:not(:checked)').length > 0);
  });
  $('input[type="checkbox"][name*="items"]').on('click', function() {
    if($(this).prop('checked') == false) {
      $('#checkAllItems').prop('checked', false);
    }
  });
  // Групповое удаление
  $('.btn-group-remove').click(function(e) {
    e.preventDefault();
    // Омечаем действие на удаление
    $('form.items input[name=action]').val('delete');
    $('form.items').submit();
  })
  // Одиночное удаление
  $('.btn-remove').click(function(e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    // Снимаем групповые отмеченные, если таковые есть
    $('input[type=checkbox][name*=items]').prop('checked', false);
    $('#checkAllItems').prop('checked', false);
    // Отмечаем item
    $('input[type="checkbox"][name*=items][value='+id+']').prop('checked', true);
    // Омечаем действие на удаление
    $('form.items input[name=action]').val('delete');
    $('form.items').submit();
  })
});